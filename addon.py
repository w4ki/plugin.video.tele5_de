# Copyright (C) 2022-2023 w4ki
#
#  This file is part of the TELE 5 Mediathek Kodi addon.
# 
#  The TELE 5 Mediathek Kodi addon is free software: you can redistribute it
#  and/or modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
# 
#  The TELE 5 Mediathek Kodi addon is distributed in the hope that it will be
#  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
#  Public License for more details.
# 
#  You should have received a copy of the GNU General
#  Public License along with the TELE 5 Mediathek Kodi
#  addon. If not, see <https://www.gnu.org/licenses/>.

import sys

PY3 = sys.version_info[0] == 3

import os.path
import xbmc, xbmcaddon, xbmcgui, xbmcplugin
import json
from uuid import uuid4 as make_uuid
import re
if PY3:
	from urllib.parse import urlsplit, urlunsplit
	from urllib.request import urlopen, Request
	from urllib.error import HTTPError
else:
	from urlparse import urlsplit, urlunsplit
	from urllib2 import urlopen, Request, HTTPError
from time import time, strftime, strptime, localtime, sleep
from calendar import timegm

#from bs4 import BeautifulSoup

KODI_VERSION = int(xbmc.getInfoLabel('System.BuildVersion').split('.')[0])

PATHS = ('main_menu', {
	'play': 'play_video',
	'movies': ('list_movies', {}),
	'series': 'show_series',
	'special': 'show_special',
	'live': ('play_live', {}),
	'settings': ('open_settings', {}),
})

COMMON_HEADERS = {
	'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:96.0) 6.0',
}

DISCO_REALM = 'dmaxde'

DISCO_HEADERS = {
	'Accept': '*/*',
	'Accept-Language': 'en-US,en;q=0.5',
	'Referer': 'https://tele5.de/',
	'X-disco-client': 'Alps:HyogaPlayer:0.0.0',
	'X-disco-params': 'realm=' + DISCO_REALM,
	'X-Device-Info': 'STONEJS/1 (Unknown/Unknown; Unknown/Unknown; Unknown)',
	'Origin': 'https://tele5.de',
	#'Sec-Fetch-Dest': 'empty',
	#'Sec-Fetch-Mode': 'cors',
	#'Sec-Fetch-Site': 'cross-site',
	#'Sec-GPC': '1',
}

DISCO_HOST = 'https://eu1-prod.disco-api.com'
DISCO_CHANNEL_ID = '626'

MANIFEST_TYPE_MAP = {
	'dash': 'mpd',
	'hls': 'hls',
}

DRM_TYPE_API = 'widevine'
DRM_TYPE_KODI = 'com.widevine.alpha'

SHOW_ONLINE_REMAINING = '0'
SHOW_ONLINE_NONE = '1'
SHOW_ONLINE_END = '2'

INPUTSTREAM_PROPERTY = \
	'inputstream' if KODI_VERSION >= 19 else 'inputstreamaddon'

class Plugin:
	def __init__(self, argv):
		url = urlsplit(argv[0])
		self.handle = int(argv[1])
		self.name = url.netloc
		self.addon = xbmcaddon.Addon(self.name)
		self.show_online = self.get_setting('showOnlineUntil')

		xbmcplugin.setContent(self.handle, 'videos')

		path = url.path.strip('/')
		paths = PATHS
		while isinstance(paths, tuple):
			if path == '':
				getattr(self, paths[0])()
				break
			else:
				head, _, path = path.partition('/')
				paths = paths[1][head]
		else:
			getattr(self, paths)(path)

	def main_menu(self):
		icon = self.get_icon()
		fanart = self.get_fanart()

		item = xbmcgui.ListItem(self.translate(30210))
		item.setInfo('video', {
			'plot': self.translate(30211),
			'mediatype': 'video',
		})
		item.setArt({
			'poster': icon,
			'fanart': fanart,
		})
		item.setProperty('isPlayable', 'true')
		self.add_dir_item('/live', item)
		for path, title in [
			('/movies', 30212),
			('/series', 30213),
			('/special', 30214),
		]:
			item = xbmcgui.ListItem(self.translate(title))
			item.setArt({
				'poster': icon,
				'fanart': fanart,
			})
			self.add_dir_item(path, item, is_folder = True)
		if self.get_setting_bool('show_settings'):
			item = xbmcgui.ListItem(self.translate(30215))
			item.setArt({
				'poster': icon,
				'fanart': fanart,
			})
			self.add_dir_item('/settings', item)
		self.end_dir()

	def open_settings(self):
		self.addon.openSettings()

	def list_movies(self):
		self.set_sort_methods()
#		if self.get_setting_bool('fetchWebImages'):
#			try: movies_page = self.fetch_soup('https://tele5.de/filme/')
#			except HTTPError:
#				self.log("Couldn't fetch movie page", xbmc.LOGERROR)
#				movies_page = None
#		else:
#			movies_page = None
		for movie in self.fetch_disco_paginated('/content/videos'
		  '?include=images,show'
		  '&filter[videoType]=STANDALONE'
		  '&filter[primaryChannel.id]='+DISCO_CHANNEL_ID):
			show = movie['relationships']['show']['data']
			if show['attributes']['videoCount'] > 1: continue
			item = self.make_video_item(movie)
			item.setInfo('video', {
				'mediatype': 'movie',
			})
#			if movies_page is not None:
#				link = movies_page.find('a',
#				  href = '/mediathek/'+movie['attributes']['alternateId']+'/')
#				if link is not None:
#					bg = link.find_parent(
#					  attrs = {'data-backgroundimage': True})
#					if bg is not None:
#						poster = bg['data-backgroundimage']
#						item.setArt({ 'poster': poster })
			self.add_dir_item('/play/' + movie['id'], item)
		self.end_dir()

	def show_series(self, series_id):
		if not series_id:
			self.list_series()
			return
		series_id, _, season = series_id.partition('/')
		if season: season_filter = '&filter[seasonNumber]=' + season
		else: season_filter = ''
		videos = list(self.fetch_disco_paginated('/content/videos'
		  '?filter[show.id]='+series_id+
		  season_filter+
		  '&include=images,show'))
		if not videos: return
		show_title = \
			videos[0]['relationships']['show']['data']['attributes']['name']

		if len(videos) > 7:
			seasons = \
				set(video['attributes']['seasonNumber'] for video in videos)
			if len(seasons) > 1:
				for season in sorted(seasons):
					title = 'Staffel '+str(season)
					item = xbmcgui.ListItem(title)
					item.setInfo('video', {
						'mediatype': 'season',
						'title': title,
						'tvshowtitle': show_title,
						'season': season,
					})
					self.add_dir_item(
						'/series/{}/{}'.format(series_id, season), item,
						is_folder = True)
				self.end_dir()
				return
		episodes = []
		for video in videos:
			if video['attributes']['videoType'] == 'EPISODE':
				episodes.append(video)
			else:
				item = self.make_video_item(video)
				item.setInfo('video', { 'mediatype': 'video' })
				self.add_dir_item('/play/' + video['id'], item)
		episodes.sort(key = lambda v: (v['attributes']['seasonNumber'],
		                               v['attributes']['episodeNumber']))
		for episode in episodes:
			item = self.make_video_item(episode)
			attrs = episode['attributes']
			season_number = attrs['seasonNumber']
			ep_number = attrs['episodeNumber']
			title = attrs['name']
			item.setLabel(
				u'S{:02}E{:02}: {}'.format(season_number, ep_number, title))
			item.setInfo('video', {
				'mediatype': 'episode',
				'title': title,
				'tvshowtitle': show_title,
				'season': season_number,
				'episode': ep_number,
			})
			self.add_dir_item('/play/' + episode['id'], item)
		self.end_dir()

	def list_series(self):
		for series in self.fetch_disco_paginated('/content/shows'
		  '?filter[primaryChannel.id]='+DISCO_CHANNEL_ID):
			if series['attributes']['episodeCount'] == 0: continue
			#if series['attributes']['videoCount'] == 0: continue
			title = series['attributes']['name']
			item = xbmcgui.ListItem(title)
			item.setInfo('video', {
				'mediatype': 'tvshow',
				'title': title,
			})
			try:
				item.setInfo('video', {
					'plot': series['attributes']['description'],
				})
			except KeyError: pass
			self.add_dir_item('/series/' + series['id'], item,
			  is_folder = True)
		self.end_dir()

	def show_special(self, series_id):
		if not series_id:
			self.list_special()
			return
		self.set_sort_methods()
		videos = list(self.fetch_disco_paginated('/content/videos'
		  '?filter[show.id]='+series_id+
		  '&include=images,show'))
		if not videos: return
		for video in videos:
			item = self.make_video_item(video)
			if video['attributes']['videoType'] == 'STANDALONE':
				mediatype = 'movie'
			else:
				mediatype = 'video'
			item.setInfo('video', {
				'mediatype': mediatype,
			})
			self.add_dir_item('/play/' + video['id'], item)
		self.end_dir()

	def list_special(self):
		for series in self.fetch_disco_paginated('/content/shows'
		  '?filter[primaryChannel.id]='+DISCO_CHANNEL_ID):
			if (series['attributes']['episodeCount'] != 0
			  or series['attributes']['videoCount'] <= 1):
				continue
			title = series['attributes']['name']
			item = xbmcgui.ListItem(title)
			item.setInfo('video', {
				'title': title,
			})
			try:
				item.setInfo('video', {
					'plot': series['attributes']['description'],
				})
			except KeyError: pass
			self.add_dir_item('/special/' + series['id'], item,
			  is_folder = True)
		for clip in self.fetch_disco_paginated('/content/videos'
		  '?include=images,show'
		  '&filter[videoType]=CLIP'
		  '&filter[primaryChannel.id]='+DISCO_CHANNEL_ID):
			show = clip['relationships']['show']['data']
			if show['attributes']['videoCount'] > 1: continue
			item = self.make_video_item(clip)
			item.setInfo('video', {
				'mediatype': 'video',
			})
			self.add_dir_item('/play/' + clip['id'], item)
		self.end_dir()

	def make_video_item(self, video_info):
		title = video_info['attributes']['name']
		item = xbmcgui.ListItem(title)
		description = video_info['attributes'].get('description')
		if self.show_online == SHOW_ONLINE_NONE:
			online = None
		else:
			try:
				online_end_timestamp = \
				  self.parse_timestamp(video_info['attributes']['publishEnd'])
			except KeyError: online = None
			else:
				if self.show_online == SHOW_ONLINE_REMAINING:
					remaining_time = online_end_timestamp - time()
					online = self.translate(30200).format(
						self.format_duration_user(remaining_time)
					)
				elif self.show_online == SHOW_ONLINE_END:
					online = self.translate(30201).format(
						self.format_timestamp_user(online_end_timestamp)
					)
		if description is None: plot = online
		elif online is None: plot = description
		else: plot = online + '\n\n' + description
		item.setInfo('video', {
			'title': title,
			'date': self.format_timestamp(self.parse_timestamp(
				video_info['attributes']['publishStart'])),
		})
		if plot is not None:
			item.setInfo('video', { 'plot': plot })
		try:
			item.setInfo('video', {
				'duration': video_info['attributes']['videoDuration'] // 1000,
			})
		except KeyError: pass
		try:
			thumb = video_info['relationships']['images']['data'][0]['attributes']['src']
		except KeyError:
			pass
		else:
			item.setArt({ 'thumb': thumb })
			if self.get_setting_bool('useThumbAsFanart'):
				item.setArt({ 'fanart': thumb })
		item.setProperty('isPlayable', 'true')
		return item

	def parse_timestamp(self, timestamp):
		return timegm(strptime(timestamp, '%Y-%m-%dT%H:%M:%SZ'))

	def format_timestamp(self, secs):
		return strftime('%d.%m.%Y', localtime(secs))

	def format_duration_user(self, duration):
		if duration < 60:
			return '{}s'.format(int(duration))
		elif duration < 3600:
			return '{}m'.format(int(duration // 60))
		elif duration < 86400:
			return '{}h'.format(int(duration // 3600))
		else:
			return '{}d'.format(int(duration // 86400))

	def format_timestamp_user(self, timestamp):
		if abs(timestamp - time()) <= 43200:
			f = xbmc.getRegion('time')
		else:
			f = xbmc.getRegion('dateshort')
		return strftime(f, localtime(timestamp))

	def play_video(self, video_id):
		import inputstreamhelper

		video_info = self.fetch_disco('/playback/v3/videoPlaybackInfo', {
			'deviceInfo': {
				'adBlocker': False,
				#'drmSupported': False,
				#'hdrCapabilities': ['SDR'],
				#'hwDecodingCapabilities': [],
				#'soundCapabilities': ['STEREO'],
			},
			#'wisteriaProperties': {},
			'videoId': str(video_id),
		})
		for stream in video_info['data']['attributes']['streaming']:
			manifest_type_api = stream['type']
			try:
				manifest_type_kodi = MANIFEST_TYPE_MAP[manifest_type_api]
			except KeyError:
				self.log("Unknown manifest type {!r}".format(manifest_type_api),
				  xbmc.LOGWARNING)
				continue

			drm = stream['protection']
			if drm['drmEnabled']:
				streamhelper = inputstreamhelper.Helper(manifest_type_kodi,
				  drm = DRM_TYPE_KODI)
			else:
				streamhelper = inputstreamhelper.Helper(manifest_type_kodi)
			if not streamhelper.check_inputstream():
				return

			item = xbmcgui.ListItem(path = stream['url'])
			item.setInfo('video', {})
			item.setProperty(INPUTSTREAM_PROPERTY,
				streamhelper.inputstream_addon)
			item.setProperty('inputstream.adaptive.manifest_type',
				manifest_type_kodi)
			if drm['drmEnabled']:
				item.setProperty('inputstream.adaptive.license_type',
					DRM_TYPE_KODI)
				item.setProperty('inputstream.adaptive.license_key',
					drm['schemes'][DRM_TYPE_API]['licenseUrl'] +
					'|PreAuthorization=' + drm['drmToken'] +
					'&Content-Type=application/octet-stream' +
					'|R{SSM}|')
			self.set_resolved(item)
			return

		self.log("No known manifest type found", xbmc.LOGERROR)
		xbmcgui.Dialog().notification(
			self.translate(30216),
			self.translate(30217),
			xbmcgui.NOTIFICATION_ERROR,
		)

	def play_live(self):
		import inputstreamhelper
		html_page = self.fetch('https://embed-zattoo.com/tele-5/?banner=false')
		token = (re.search(r"appToken = '([a-z0-9]*)'".encode('ascii'),
			html_page).group(1).decode('ascii'))
		uuid = make_uuid()
		result = \
			self.fetch_json_post_raw('https://embed-zattoo.com/zapi/watch',
				'session_token={}&uuid={}&device_type=&partner_site=tele5'
				  '&cid=tele-5&stream_type=dash&https_watch_urls=True'
				  .format(token, uuid).encode('ascii'),
				headers = {
					'Origin': 'https://embed-zattoo.com',
					'Referer': 'https://embed-zattoo.com/tele-5/?banner=false',
					'Content-Type':
						'application/x-www-form-urlencoded; charset=UTF-8',
					'Accept': 'application/json',
				})
		url = result['stream']['url']
		streamhelper = inputstreamhelper.Helper('mpd')
		if not streamhelper.check_inputstream(): return
		item = xbmcgui.ListItem(path = url)
		item.setInfo('video', {})
		item.setProperty(INPUTSTREAM_PROPERTY, streamhelper.inputstream_addon)
		item.setProperty('inputstream.adaptive.manifest_type', 'mpd')
		self.set_resolved(item)

	def fetch_disco_paginated(self, path):
		if '?' in path: path += '&'
		else: path += '?'
		path += 'page[size]=100'
		first_page = self.fetch_disco(path)
		for item in first_page['data']: yield item
		for page in range(2, first_page['meta']['totalPages']+1):
			result = self.fetch_disco('{}&page[number]={}'.format(path, page))
			for item in result['data']: yield item

	def fetch_disco(self, path, data = None, headers = {}):
		token = self.get_disco_token()
		all_headers = DISCO_HEADERS.copy()
		all_headers['Authorization'] = 'Bearer '+token
		all_headers.update(headers)
		response = self.fetch_json(DISCO_HOST + path, data, all_headers)
		if 'included' in response:
			included = {(a['type'], a['id']): a
				for a in response['included']
			}
			if included:
				for inc in included.values():
					self.update_included(inc, included)
				self.update_included(response['data'], included)
		return response

	def update_included(self, data, included):
		def find_value(v):
			try: return included[v['type'], v['id']]
			except KeyError: return v

		def update_item(item):
			try: relations = item['relationships']
			except KeyError: return
			for value in relations.values():
				data = value['data']
				if isinstance(data, list):
					for i, item in enumerate(data): data[i] = find_value(item)
				else:
					value['data'] = find_value(data)

		if isinstance(data, list):
			for item in data: update_item(item)
		else:
			update_item(data)

	def fetch_json(self, url, data = None, headers = {}):
		if data is not None: data = json.dumps(data).encode('ascii')
		all_headers = {'Content-Type': 'application/json'}
		all_headers.update(headers)
		return self.fetch_json_post_raw(url, data, headers)

	def fetch_json_post_raw(self, url, data = None, headers = {}):
		return self.urlopen(url, data, headers, json.load)

#	def fetch_soup(self, url, data = None, headers = {}):
#		return self.urlopen(url, data, headers,
#		  lambda f: BeautifulSoup(f, 'html.parser'))

	def fetch(self, url, data = None, headers = {}):
		return self.urlopen(url, data, headers, lambda f: f.read())

	def urlopen(self, url, data, headers, func):
		all_headers = COMMON_HEADERS.copy()
		all_headers.update(headers)
		request = Request(url, data, headers = all_headers)
		f = urlopen(request)
		try:
			return func(f)
		finally:
			f.close()

	def get_disco_token(self):
		try: return self.disco_token
		except AttributeError: pass
		response = self.fetch_json(
			DISCO_HOST+'/token?realm='+DISCO_REALM,
			headers = DISCO_HEADERS
		)
		token = response['data']['attributes']['token']
		self.disco_token = token
		return token

	def log(self, string, level):
		xbmc.log('[{}] {}'.format(self.name, string), level)

	def set_sort_methods(self):
		xbmcplugin.addSortMethod(self.handle, xbmcplugin.SORT_METHOD_DATE)
		xbmcplugin.addSortMethod(self.handle, xbmcplugin.SORT_METHOD_TITLE)

	def add_dir_item(self, path, listitem, is_folder = False):
		path = urlunsplit(('plugin', self.name, path, '', ''))
		listitem.setPath(path)
		xbmcplugin.addDirectoryItem(self.handle, path, listitem, is_folder)

	def end_dir(self):
		xbmcplugin.endOfDirectory(self.handle)

	def set_resolved(self, listitem):
		listitem.setProperty('isPlayable', 'true')
		listitem.setContentLookup(False)
		xbmcplugin.setResolvedUrl(self.handle, True, listitem)

	def translate(self, num):
		return self.addon.getLocalizedString(num)

	def get_setting(self, name):
		return self.addon.getSetting(name)
	def get_setting_bool(self, name):
		return self.get_setting(name) == 'true'

	def get_icon(self):
		return self.addon.getAddonInfo('icon')
	def get_fanart(self):
		return self.addon.getAddonInfo('fanart')

Plugin(sys.argv)
